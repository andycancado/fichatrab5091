using System;

namespace jogo_galo

{
    class Program

    {
        static char jogador;


        static char[,] Jogada(char[,] tabela, int linha, int coluna, char op)

        {//modifica o array com as posicoes e x ou o
            tabela[linha, coluna] = op;
            return tabela;
        }

        static char[,] PreencherTudo(char[,] tabela)

        {//imprime na consola a metriz
            int pos = 0;
            Console.Clear();
            Console.WriteLine("\n   JOGO DO GALO");
            Console.WriteLine(" \n  ---------------");
            for (int i = 0; i < tabela.GetLength(0); i++)

            {
                Console.WriteLine(" \n  ---------------");
                for (int j = 0; j < tabela.GetLength(1); j++)

                {

                    pos = i + j + (i * 2);

                    if (tabela[i, j] != 'x' && tabela[i, j] != 'o')

                    {
                        tabela[i, j] = (char)(pos + 49);

                    }
                    if (tabela[i, j] == 'x')

                        Console.ForegroundColor = ConsoleColor.Red;
                    if (tabela[i, j] == 'o')

                        Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("  |{0}| ", tabela[i, j]);
                    Console.ForegroundColor = ConsoleColor.Gray;
                }
            
            }
            Console.WriteLine(" \n  ---------------");
            return tabela;
        }

        static bool Verificar(char[,] tabela, int linha, int coluna)

        {//verifica se a jogada e valida
            if (linha > 2 || coluna > 2 || linha < 0 || coluna < 0)

                return false;
            if (tabela[linha, coluna] == 'x' || tabela[linha, coluna] == 'o')

                return true;
            return false;
        }

        static bool VerFinal(char[,] tabela)

        {// verificar se jogador ganhou
            int i, j;
            char[] linha = new char [3];
            char[] coluna = new char[3];
            char[] diagonal = new char[3];
            char[] diagonal2 = new char[3];

            for (i = 0; i < tabela.GetLength(0); i++)

            {         
                for (j = 0; j < tabela.GetLength(1); j++)

                {
                    linha[j] = tabela[i, j];                  
                    if (i == j)

                        diagonal[i] = tabela[i, j];
                    diagonal2[j] = tabela[j, 2 - j];
                    coluna[j] = tabela[j, i];
                }

                if (linha[0] == linha[1] && linha[2] == linha[0])
                    return true;
                if (coluna[0] == coluna[1] && coluna[2] == coluna[0])

                    return true;
                if (diagonal[0] == diagonal[1] && diagonal[2] == diagonal[0])

                    return true;
                if (diagonal2[0] == diagonal2[1] && diagonal2[2] == diagonal2[0])

                    return true;             
            }

            return false;
        }

        static char TrocaJogador()

        {//alternar jogador
            if (jogador == 'x')

                return jogador = 'o';

            return 'x';
        }

        static void JogoGalo()

        {
            char[,] jogo = new char[3, 3];
            char op;
            int linha = 0, coluna = 0, cont = 0;
            bool final;
            
            do

            {
                cont++;
                TrocaJogador();
                jogo = PreencherTudo(jogo);
                Console.WriteLine("insira a posiçao onde quer jogar? (1 a 9)---> {0} ", jogador);             
                op = Console.ReadKey().KeyChar;

                switch (op)

                {
                    case '1':                       
                        linha = 0;
                        coluna = 0;
                        break;
                    case '2':
                        linha = 0;
                        coluna = 1;
                        break;
                    case '3':
                        linha = 0;
                        coluna = 2;
                        break;
                    case '4':
                        linha = 1;
                        coluna = 0;
                        break;
                    case '5':
                        linha = 1;
                        coluna = 1;
                        break;
                    case '6':
                        linha = 1;
                        coluna = 2;
                        break;
                    case '7':
                        linha = 2;
                        coluna = 0;
                        break;
                    case '8':
                        linha = 2;
                        coluna = 1;
                        break;
                    case '9':
                        linha = 2;
                        coluna = 2;
                        break;
                }

                if (Verificar(jogo, linha, coluna))

                {
                    TrocaJogador();
                    cont--;
                }

                else

                {
                    jogo = Jogada(jogo, linha, coluna, jogador);
                }

                PreencherTudo(jogo);
                final = VerFinal(jogo);

                if (cont > 9)

                {
                    Console.WriteLine("empate.....");
                    final = false;
                    break;
                }
                
            } while (!final);

            if (final)

                Console.WriteLine("o jogador '{0}' ganhou!!!!!!!!", jogador);
        }

        static void Main(string[] args)

        {
            JogoGalo();

        }
    }
}
