﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex4
    {
        private static float Divisao(int num1, int num2)
        {
            return (float)num1 / (float)num2;
        }

        public static void ExDiv()
        {
            Console.Clear();
            Console.WriteLine("divisao de dois numero");
            Console.WriteLine("insira um numero");
            int numero1 = int.Parse(Console.ReadLine());
            Console.WriteLine("insira outro numero");
            int numero2 = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} / {1} = {2}", numero1, numero2, Divisao(numero1, numero2));
            Console.ReadKey();

        }
    }
}
