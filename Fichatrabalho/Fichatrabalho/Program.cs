﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Program
    {
        public static void Menu()
        {
            Console.Clear();
            Console.WriteLine("Escolha um excercicio: ");
            //escrever o menu na consola
            string[] op_menu = new string[16] { "soma de 2 numeros","subtracao de 2 numeros",
                            "multplicacao 2 numeros","divisao 2 numeros","soma dos numeros de 1 ate 10",
                            "Soma de numeros de 1 ate N","tabuada ate 10","tabuada de um numero",
                            "calcular media idade 10 pessoas","conversor decimal para binario",
                            "Conversor decimal para hexadecimal","verificar se ano é bissesto",
                            "media nota de semestre","Poligonos","Calculador de dias de vida",
                            "Conversor graus Fahrenheit para celcius" };
            for (int i = 0; i < op_menu.Length; i++)
            {
                Console.WriteLine("{0} >>>> {1}", i+1, op_menu[i]);
            }
            Console.WriteLine("s- Sair");
            string esc = Console.ReadLine();

            switch (esc)
            {
                case "1"://soma de 2 numeros
                    Ex1.ExSoma();
                    Menu();
                    break;

                case "2":// subtracao de 2 numeros
                    Ex2.ExSub();
                    Menu();
                    break;

                case "3"://multplicacao 2 numeros
                    Console.Clear();
                    Ex3.ExMult();
                    Menu();
                    break;

                case "4"://divisao 2 numeros
                    Console.Clear();
                    Ex4.ExDiv();
                    Menu();
                    break;

                case "5"://soma dos numeros de 1 ate 10
                    Console.Clear();
                    Ex5.Exercicio5();
                    Menu();
                    break;

                case "6"://Soma de numeros de 1 ate N
                    Ex6.SomaN();
                    Menu();
                    break;
                case "7"://tabuada ate 10
                    Console.Clear();
                    Ex7.Tab();
                    Menu();
                    break;
                case "8": //tabuada de um numero
                    Ex8.TabN();
                    Menu();
                    break;
                case "9": // calcular media idade 10 pessoas
                    Ex9.MediaIdades();
                    Menu();
                    break;

                case "10": //conversor decimal para binario
                    Ex10.ConvBinario();
                    Menu();
                    break;

                case "11": //Conversor decimal para hexadecimal
                    Ex11.ConvHex();
                    Menu();
                    break;
                case "12"://verificar se ano é bissesto
                    Ex12.main();
                    Menu();
                    break;
                case "13":// media nota de semestre
                    Ex13.main();
                    Menu();
                    break;
                case "14":// Poligonos
                    Ex14.main();
                    Menu();
                    break;
                case "15":// Calculador de dias de vida
                    Ex15.main();
                    Menu();
                    break;
                case "16"://Conversor graus Fahrenheit para celcius
                    Ex16.main();
                    Menu();
                    break;



                case "s":
                    break;

                default:
                    Menu();
                    break;
            }
        }


        static void Main(string[] args)
        {
            Menu();
        }
    }
}
