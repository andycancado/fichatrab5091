﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex6
    {    //somatorio de numeros de 1 ate N

        private static int SomaDez(int num, int inc=1)
        {

            if (inc == num) return inc;
            return inc++ + SomaDez(num, inc);

        }

        public static void SomaN()
        {
            Console.WriteLine("Soma de 1 ate N");
            Console.WriteLine("insira um numero inteiro:");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("Soma de 1 ate {0} = {1}", num, SomaDez(num));
            Console.ReadKey();
        }
    }
}
