﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex8
    {
        //tabuada de um numero N

        public static void TabN()
        {
            Console.Clear();
            Console.WriteLine("Insira um numero ate 10");
            int num = int.Parse(Console.ReadLine());

           
                for (int c = 1; c <= 10; c++)
                {
                    Console.WriteLine(" {0} + {1} = {2}", c, num, c + num);
                }
            Console.ReadKey();

        }
    }
}
