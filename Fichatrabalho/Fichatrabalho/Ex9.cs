﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex9
    {   //calcular media de idade de 10 pessoas
        public static void MediaIdades()
        {
            char genero;
            int idadeM = 0;
            int idadeF = 0;
            int masc = 0;
            for (int i = 1; i <=10; i++)
            {
                Console.Clear();
                Console.WriteLine("\n{0} Qual a sua idade:", i);
                int idade = int.Parse(Console.ReadLine());
                do
                {
                    Console.WriteLine("Indique (m) Masculino ou (f) Femenino:");
                    genero = Console.ReadKey().KeyChar;

                } while (genero != 'm' && genero != 'f');

                if (genero == 'm')
                    {
                        idadeM += idade;
                        masc++;
                    }
                else
                    {
                        idadeF += idade;
                    }
            }
            Console.WriteLine("Media de idade total = {0} anos", float.Parse((idadeF + idadeM).ToString()) / 10);
            Console.WriteLine("Media de idade Masculino = {0} anos", float.Parse(idadeM.ToString()) / masc);
            Console.WriteLine("Media de idade Femenino = {0} anos", float.Parse(idadeF.ToString()) / 10 - masc);
            Console.ReadKey();


        }

    }
}
