﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex13
    {
        //calcular media de semestre
        public static bool validarNota(float nota)
        {
            if (nota >= 0 && nota <= 20) return true;
            else return false;
        }
        public static void notaAluno(float nota1, float nota2)
        {
            if (validarNota(nota1) && validarNota(nota2))
            {
                float media = (nota1 + nota2) / 2;
                if (media >= 12) Console.WriteLine("Parabens, passou com média final de {0} valores", media);
                else Console.WriteLine("Média {0} é insuficiente!", media);
            }
            else
                Console.WriteLine("nota invalida, 0 a 20");

        }

        public static void main()
        {
            Console.Clear();
            Console.WriteLine("Insira a nota da primeira avaliação");
            float nota1 = float.Parse(Console.ReadLine());
            Console.WriteLine("Insira a nota da segunda avaliação");
            float nota2 = float.Parse(Console.ReadLine());
            notaAluno(nota1, nota2);
            Console.ReadKey();

        } 
    }
}
