﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex14
    {
        //calcular areas e perimetros de poligonos
        public static void Poligono(int lados, int medida)
        {
            switch (lados)
            {
                case 3:

                    Console.WriteLine("TRIANGULO, o perimetro é {0}", 3 * medida);
                    break;
                case 4:
                    {
                        Console.WriteLine("QUADRADO, area é {0}", medida ^ 2);
                        break;
                    }
                case 5:
                    Console.WriteLine("PENTAGONO");
                    break;
                default:
                    break;
            }

        }

        public static void main()
        {
            Console.Clear();
            Console.WriteLine("Insira o numero de lados do poligono:");
            int lados = int.Parse(Console.ReadLine());
            Console.WriteLine("Insira a medida dos lados:");
            int medida = int.Parse(Console.ReadLine());
            Poligono(lados, medida);
            Console.ReadKey();
        }
    }
}
