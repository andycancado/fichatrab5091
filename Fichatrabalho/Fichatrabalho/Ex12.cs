﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex12
    {
        //verificar se ano é bisexto

        public static bool eBissesto(int ano)
        {
                  
                 if ((ano % 4 == 0 && ano % 100 != 0) || ano % 400 == 0) return true;
                else return false;          
            
        }

        public static void main()
        {
            Console.Clear();
            int ano;
            Console.WriteLine("Insira um ano para saber se é bisexto: (> 400):");
            ano = int.Parse(Console.ReadLine());
            if (ano < 400) Console.WriteLine("O ano introduzido é invalido");
            else
                if (eBissesto(ano)) Console.WriteLine("{0} é bisexto", ano);
                else Console.WriteLine("{0} não é bisexto", ano);
            Console.ReadKey();
        }

    }
}
