﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex2
    {

        private static int Subtracao(int num1, int num2)
        {

            if ( num1 < num2)
            {
                return num2 - num1;
            }
            return num1 - num2;
        }

        public static void ExSub()
        {
            Console.Clear();
            Console.WriteLine("Subtracao de dois numero");
            Console.WriteLine("insira um numero");
            int numero1 = int.Parse(Console.ReadLine());
            Console.WriteLine("insira outro numero");
            int numero2 = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} - {1} = {2}", numero1, numero2, Subtracao(numero1, numero2));
            Console.ReadKey();

        }
    }
}
