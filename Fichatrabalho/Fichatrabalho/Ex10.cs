﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{   
    class Ex10
    {
        
        public static string IntParaBinario(int num)
        {   //Converter interio para binario
            if (num < 2)
                return num.ToString();
            else
            {
                int div = num / 2;
                int resto = num % 2;
                return IntParaBinario(div) + resto;

            }

        }


        public static void ConvBinario()
        {
            Console.Clear();
            Console.WriteLine("Insira um inteiro para converter para binario:");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} >>>>> {1}", num, IntParaBinario(num));
            Console.ReadKey();
        }

    }
}
