﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex11
    {
        //conversor de decimal para hexadecimal
        public static string Hexa(int inthexa)
        {
            if (inthexa < 10) return inthexa.ToString();
            else
            {
                switch (inthexa)
                {
                    case 10:
                        return "A";
                        break;
                    case 11:
                        return "B";
                        break;
                    case 12:
                        return "C";
                        break;
                    case 13:
                        return "D";
                        break;
                    case 14:
                        return "E";
                        break;
                    case 15:
                        return "F";
                        break;
                    default:
                        return "";
                        break;

                }


            }


        }


        public static string IntParaHex(int num)
        {   //Converter interio para hexa

            if (num <= 15)
                return Hexa(num);
            else
            {

                int div = num / 16;
                int resto = num % 16;
                return IntParaHex(div) + Hexa(resto);
            }


        }
        public static void ConvHex()
        {
            Console.Clear();
            Console.WriteLine("Insira um inteiro para converter para hexadecimal:");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} >>>>> {1}", num, IntParaHex(num));
            Console.ReadKey();
        }
    }
}
