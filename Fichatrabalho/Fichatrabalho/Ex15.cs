﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex15
    {
        //calcular anos e dias de vida
        public static int diasMes(int mes, int ano)
        {   //retorna os dias que cada mes tem
             if (mes == 2)
            {
                 if (Ex12.eBissesto(ano))
                {
                    return 29;
                }
                else return 28;
            }
            if (mes % 2 == 0)
            {
                if (mes < 8) return 30;

                else return 31;
            }
            else
            {
                if (mes > 8) return 30;           
                else return 31;
            }        

        }

        public static int diasAno(int ano)
        {   //retorna os dias que cada ano tem
            if (Ex12.eBissesto(ano))
            {
                return 366;
            }
            else
            {
                return 365; 
            }

        }

        public static void diasVida(string aniv)
        {
            int dias = 0;
            int ano = int.Parse(aniv.Split('-', ' ')[2]);
            int mes = int.Parse(aniv.Split('-', ' ')[2]);
            int dia = int.Parse(aniv.Split('-', ' ')[2]);
            string data = DateTime.Today.ToString();
            string[] datas = data.Split('-', ' ');
            //adiciona os anos completos
            for (int i = ano + 1  ; i < int.Parse(datas[2]) ; i++)
            {
                dias += diasAno(i);
                    
            }
            //adiciona os dias dos meses completos do ano de nascimento
            for (int i = mes; i <12 ; i++)
            {
                dias += diasMes(i, ano);
            }

            //adiciona os dias do ano corrente
            for (int i = 1;  i < int.Parse(datas[1]) ; i++)
            {
                dias += diasMes(i, ano);
            }
            
            // adiciona os dias apartir do nascimento ate o final do mes
            dias += diasMes(mes, ano) - dia;

            // adiciona os dias do mes corrente
            dias += int.Parse(datas[0]);

            int anos = int.Parse(datas[2]) - ano;
            int meses;
            if (mes >= int.Parse(datas[1]) )
            {
                meses = int.Parse(datas[1]);
            }
            else meses = int.Parse(datas[1]) - mes;

            Console.WriteLine("Voce tem {0} anos, {1} meses, {2} dias ", anos, meses, int.Parse(datas[0]));
            
            Console.WriteLine("Tem {0} dias total de vida",dias);
            Console.ReadKey();
        }

        public static void main()
        {
            Console.Clear();
            Console.WriteLine("Insira a sua data de nascimento:(dd-mes-ano)");
            
            string aniv = Console.ReadLine();
            Console.WriteLine(aniv.SequenceEqual("00-00-0000"));
            if (validaAniv(aniv)) diasVida(aniv);
            else Console.WriteLine("Data invalida");
            Console.ReadKey();
        }

        public static bool validaAniv(string aniv)
        {
            if (aniv.Length != 10 && aniv[2] != '-' && aniv[5] != '-') return false;
            else return true;
        }

    }


}
