﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex3
    {

        private static int Multiplicacao(int num1, int num2)
        {

            
            return num1 * num2;
        }

        public static void ExMult()
        {
            Console.WriteLine("Multiplicacao de dois numero");
            Console.WriteLine("insira um numero");
            int numero1 = int.Parse(Console.ReadLine());
            Console.WriteLine("insira outro numero");
            int numero2 = int.Parse(Console.ReadLine());
            Console.WriteLine("{0} * {1} = {2}", numero1, numero2, Multiplicacao(numero1, numero2));
            Console.ReadKey();

        }
    }
}
