﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{   //tabuada ate 10
    class Ex7
    {
        public static void Tab(int num = 1)
        {

            for (int i = 1; i <= 10; i++)
            {
                for (int c = 1; c <= 10; c++)
                {
                    Console.Write("  {0}+{1}={2}", i, c, c + num);
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

    }
}
