﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex5
    {    //somatorio de numeros ate 10

        private static int SomaDez(int num=1)
        {
           
            if (num == 10) return num;
            return num++ + SomaDez(num);

        }

        public static void Exercicio5()
        {
            Console.WriteLine("somatorio de 1 ate 10 = {0}", SomaDez());
            Console.ReadKey();
        }
    }
}
