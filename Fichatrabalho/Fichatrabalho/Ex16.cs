﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fichatrabalho
{
    class Ex16
    {
        //conversor temperatura
        public static float ConversorTemp(float fahr)
        {
            float gCelcius;
            gCelcius = ((fahr - 32) / 9) * 5;
            return gCelcius;

        }

        public static void main()
        {
            Console.Clear();
            Console.WriteLine("Insira a temperatura em graus Fahrenheit:");
            float temp = float.Parse(Console.ReadLine());
            Console.WriteLine("{0} Fahrenheit ----> {1} Celcius", temp, ConversorTemp(temp));
            Console.ReadKey();
        }

    }
}
